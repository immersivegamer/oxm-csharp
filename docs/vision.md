# The Vision of OXM Project

A tool enable the rapid development of user applications.

The initial vision of the OXM project is to make the first pass development of an application that involves a user interface and user interaction easy, for desktop or web. So easy that the developer doesn't have to even think about it.

## Project's Name

This is why currently the project's name used the letters OXM. With data access layers there is the term ORM: Object Relational Mapper. An ORM allows a developer to map most plain old code objects (POCOs) to a relational database storage. OXM: Object eXperience Mapper, a framework to allow developers to work in POCOs that can be mapped to a full user experience.

The use of the tool is for rapid development. Create objects that describe the data and behavior for an application and the user side is rendered completely workable, but perhaps not as appealing. Initial version of the framework will focus on a solid mapping of object to building blocks to allow a rendering engine to render the parts. The first prototype works well with a basic rendering engine that uses the library Terminal.Gui to render to the console. In the future an HTML/web engine will be made so that the same rapid development can be done, though any other rendering engines could be written and used.