# Bucket List Ideas

Any ideas related to this project that should be noted.

## Arrays Should Show as Scroll Enabled Lists

See GitLab issue for more details.

## Properties that are Classes / Structs

Should show framed fields and commands as part of the parent object.

## Decouple FieldX and ActionX Parsing from Navigator

Instead inject the parsers.

## Create Generic Field Parsing

Other serializing frameworks do this, an example being JSON.Net. Rendering Engines can use this as the base to allow most fields to be displayed and edited as text fields.

## Have label overrides to enable changing labels

For example Namespace.Class.Property = "Label Text".

Currently by default the render engine uses the property or method's name as the UI element's label. Allowing overrides for the labels allows better user facing text and also the ability to do language localization. Language files could be created and then loaded on startup or even at run time (would require re-creating the current view).

Do we inject a label loop up service into IRenderX to do a label look up at time of rendering or should the parser that creates the X elements include a label and do a look up at time of parsing? Leaning to the later.IRenderX should really be focused on rendering the elements passed to it.

Was thinking about providing a localization service for objects to pass their text through. Perhaps a the label look up service can fall back on the localization service if it is provided? This way simple text does not have to translated?

## Styling Engine to Modify How Rendering Engine Displays Content

Since it is not the job of the POCO to carry information on how to display it's self if users of the tool/framework want more control over display options a modified version of CSS can be used. Render engines would have access to the hierarchy of the navigation but supporting styling and alternative controls would be optional. A basic CSS parser would be available but other formats / resources could be used with the CSS basic parser tool or replace it. It is the render engine's responsibility to consider configurable rendering. However, implementing styling would greatly improve the looks when time is put in by the app developers. 

```css
// matches any views of the same name, lower cased
name {
    // any options the rendering engine wants to support
    option-a: value
}

// more specific styles override the broader ones
// these options only apply when view B is shown
// from view A
view_a.view_b {
    option-b: value
}

// fields and commands can be specified by adding 
// 'field' or 'command' in front of the name
field.text {
    // options for any property named text
    text-color: red
}

command.save {
    text-weight: bold
}

// again more specific styles override 
view_a.field.text {
    // all properties named text under object view_a
    // will be read only even if there is a public setter
    can-edit: false
}

view_b.command.save {
    border-color: green
}

// nesting of styles is recommended for grouped styling
view_a {
    view_b { 
        option-b: value
    }
    field {
        text {
            text-color: red
        }
    }
    // or could be flat
    field.text {
        text-color: red
    }
}
```