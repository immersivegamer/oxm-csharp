# oxm-csharp

Create user interfaces just object models with close to no mapping. This is a prototype that uses Terminal.Gui as the rendering engine. Future state would include a HTML or JSON REST API.