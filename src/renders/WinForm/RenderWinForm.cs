﻿using Oxm;
using Oxm.XPart;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Oxm.Render.WinForm
{
    class WinFormRenderEngine : IRenderX
    {
        private Form mainForm;
        private TableLayoutPanel leftTable;
        private FlowLayoutPanel rightLayout;
        private DataGridView bottomGrid;

        private readonly List<Action> saveActions = new List<Action>();
        private bool hasWarnings;

        public WinFormRenderEngine(Form mainForm)
        {
            this.mainForm = mainForm;
        }

        public void AddActions(ActionX[] actionX)
        {
            foreach(var action in actionX)
            {
                var button = new Button() { Text = action.Name, AutoSize = true };
                button.Click += (o, e) =>
                {
                    var success = SaveChanges();                    
                    if(success)
                        action.Execute();
                };
                rightLayout.Controls.Add(button);
            }
        }

        private bool SaveChanges()
        {
            ClearWarnings();
            foreach (var save in saveActions)
                save();
            return !hasWarnings;
        }

        private void ClearWarnings()
        {
            hasWarnings = false;
        }

        public void AddFields(FieldX[] fieldX)
        {
            saveActions.Clear();

            for(int i = 0; i < fieldX.Length; i++)            
            {
                var field = fieldX[i];                
                var label = new Label() { Text = field.Name, AutoSize = true };
                // todo: different controls based on fieldx data type
                var input = new TextBox { Text = field.Value().ToString(), ReadOnly = !field.HasSetter, Dock = DockStyle.Fill};
                if (field.HasSetter)
                {
                    saveActions.Add(() =>
                        {
                            if(input.Modified)
                            {
                                try
                                {
                                    field.Setter(Convert.ChangeType(input.Text, field.Type));
                                }
                                catch (Exception ex)
                                {
                                    AddWarning(field, ex.Message);
                                }
                            }
                       }
                    );
                }

                leftTable.Controls.Add(label);
                leftTable.SetColumn(label, 0);
                leftTable.SetRow(label, i);

                leftTable.Controls.Add(input);
                leftTable.SetColumn(input, 1);
                leftTable.SetRow(input, i);
            }
        }

        private void AddWarning(FieldX field, string message)
        {
            // todo: warnings next to fields, highlight
            MessageBox.Show(message, $"Conversion Error - {field.Name}", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            hasWarnings = true;
        }

        public void AddLists(ListX[] listX)
        {
            if (listX.Length > 0)
                bottomGrid.Visible = true;

            // todo: populate grid
        }

        public void NewView(string name)
        {
            // do we need to do anything here?

            FirstTimeRender();

            mainForm.Text = name;
            foreach (Control child in mainForm.Controls)
            {
                mainForm.Controls.Remove(child);
                child.Dispose();
            }

            leftTable = new TableLayoutPanel
            {
                AutoScroll = true,
                Dock = DockStyle.Fill
            };

            rightLayout = new FlowLayoutPanel
            {
                AutoScroll = true,
                Dock = DockStyle.Fill,
                FlowDirection = FlowDirection.TopDown
            };

            bottomGrid = new DataGridView
            {
                //bottomGrid.Visible = false;
                Dock = DockStyle.Fill
            };

            var mainTable = new System.Windows.Forms.TableLayoutPanel
            {
                Dock = DockStyle.Fill
            };

            mainTable.Controls.Add(leftTable);
            mainTable.SetColumn(leftTable, 0);
            mainTable.SetRow(leftTable, 0);

            mainTable.Controls.Add(rightLayout);
            mainTable.SetColumn(rightLayout, 1);
            mainTable.SetRow(rightLayout, 0);

            mainTable.Controls.Add(bottomGrid);
            mainTable.SetColumn(bottomGrid, 0);
            mainTable.SetRow(bottomGrid, 1);
            mainTable.SetColumnSpan(bottomGrid, 2);

            mainTable.ColumnStyles.Add(new ColumnStyle { SizeType = SizeType.Percent, Width = 50 });
            mainTable.ColumnStyles.Add(new ColumnStyle { SizeType = SizeType.AutoSize, Width = 25 });

            mainTable.RowStyles.Add(new RowStyle { SizeType = SizeType.Percent, Height = 50 });
            mainTable.RowStyles.Add(new RowStyle { SizeType = SizeType.AutoSize });

            mainTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            mainForm.Controls.Add(mainTable);
        }

        private void FirstTimeRender()
        {
            // create form if none already
            if (mainForm == null)
            {
                mainForm = new Form();
                // todo: add menu items?
                //Application.Run(mainForm);
            }
        }

        public void Render()
        { 
            // mainForm.Show();
        }
    }
}
