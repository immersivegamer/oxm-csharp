using Oxm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oxm.Render.WinForm
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var mainForm = new Form
            {
                Width = 800,
                Height = 800
            };
            var engine = new Oxm.Render.WinForm.WinFormRenderEngine(mainForm);
            var nav = new Navigator(engine, GetExampleRecord());
            mainForm.Load += (o, e) => nav.Start();
            Application.Run(mainForm);            
        }

        private static ExampleRecord GetExampleRecord()
        {
            return new ExampleRecord
            {
                CreatedAt = DateTime.Now,
                CreatedBy = Guid.NewGuid(),
                ID = 1,
                Name = "HenryN",
                Other = "",
                ExtraThingWithAReallyLongName = new SubRecord
                {
                    ID = 2,
                    Name = "Item"
                }
            };
        }
    }

    public class ExampleRecord
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Other { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }

        public SubRecord ExtraThingWithAReallyLongName {get; set; }

        public SubRecord ViewExtaItem() => ExtraThingWithAReallyLongName;

        public void Save()
        {

        }
    }

    public class SubRecord
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
}
