using System;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{
    public interface IRTFieldMapper
    {
        Type Type { get; }
        bool RequiresLabel { get; }
        View CreateField(FieldX field, RenderTerminal renderT);
    }
}
