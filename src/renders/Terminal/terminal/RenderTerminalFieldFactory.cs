using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{ 
    public class RenderTerminalFieldFactory
    {
        private readonly RenderTerminal renderT;
        readonly Dictionary<Type, IRTFieldMapper> mappers = new Dictionary<Type, IRTFieldMapper>();
        public RenderTerminalFieldFactory(RenderTerminal renderT)
        {
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsClass).Where(x => typeof(IRTFieldMapper).IsAssignableFrom(x));
            mappers = types.Select(x => (IRTFieldMapper)Activator.CreateInstance(x)).ToDictionary(x => x.Type);
            this.renderT = renderT;
        }

        public RenderTerminalFieldFactory()
        {
        }

        public View Create(FieldX field)
        {
            if (mappers.ContainsKey(field.Type))
                return mappers[field.Type].CreateField(field, renderT);
            else
                return new Label($"Missing Type: {field.Type.Name}");
        }

        public bool CheckNeedsLabel(FieldX field) =>
            mappers.ContainsKey(field.Type) ? mappers[field.Type].RequiresLabel : false;
    }
}
