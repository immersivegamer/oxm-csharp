﻿using System;
using System.Collections.Generic;
using NStack;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{

    public class RenderTerminal : IRenderX
    {
        private Toplevel pendingTop;
        private Toplevel currentTop;
        private Window win;
        private View lastViewCreated = null;

        public RenderTerminal()
        {
            mapper = new RenderTerminalFieldFactory(this);
            Application.Init();
            currentTop = Application.Top;
        }

        public void AddActions(ActionX[] actions)
        {
            foreach (var action in actions)
            {
                Button button = ConvertToButton(action);
                SetPosBelowLast(button);
                AddView(button);
            }
        }

        private Button ConvertToButton(ActionX action)
        {
            return new Button(action.Name)
            {
                Clicked = () =>
                {
                    Save();
                    if (action.Type == ActionXType.Command)
                    {
                        action.Execute();
                        Refresh();
                    }
                    else if (action.Type == ActionXType.View)
                    {
                        action.Execute();
                    }
                    else
                    {
                        throw new NotImplementedException("Actions besides Command and View are not yet supported.");
                    }
                }
            };
        }

        public void AddFields(FieldX[] fields)
        {
            foreach (var field in fields)
            {
                View thing = mapper.Create(field);

                if(mapper.CheckNeedsLabel(field)) {
                    Label label = ConvertToLabel(field);
                    SetPosBelowLast(label);
                    thing.Y = Pos.Top(label);
                    thing.X = Pos.Right(label);
                    AddView(thing);
                    // todo: for some reason views that use Y = Pos.Bottom() 
                    //       on var 'thing' view doesn't work (i.e when 
                    //       SetPosBelowLast is called a new view object)?
                    //       hacking it by adding 'label' to win after 'thing'
                    //       so that next view objects added are based off of 
                    //       'label' instead of 'thing'.
                    AddView(label);
                }
                else
                {
                    SetPosBelowLast(thing);
                    AddView(thing);
                }
            }
        }

        private void SetPosBelowLast(View item)
        {
            item.Y = Pos.Bottom(lastViewCreated);
            item.X = 0;
        }

        private void AddView(View item)
        {
            win.Add(item);
            lastViewCreated = item;
        }

        private void ResetXYPos()
        {
            lastViewCreated = new View(new Rect(0, 0, 0, 0));
            win.Add(lastViewCreated);
        }

        private Label ConvertToLabel(FieldX field)
        {
            var label = new Label(field.Name);
            return label;
        }        

        public void NewView(string name)
        {
            SaveActions.Clear();
            SyncActions.Clear();
            pendingTop = new Toplevel();
            win = new Window(name)
            {
                X = 0,
                Y = 1,
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };
            pendingTop.Add(win);
            ResetXYPos();
            pendingTop.Add(new MenuBar(new MenuBarItem[] {
                new MenuBarItem("App",
                    new MenuItem[] {
                        new MenuItem("Quit", "", () => { 
                            this.Quit();
                        })
                    }
                )
            }));
        }

        private void Quit() {
            currentTop.Running = false;
            pendingTop.Running = false;
            Application.RequestStop();
        }

        public void Render()
        {
            currentTop.Running = false;
            currentTop = pendingTop;
            RenderLists();
            Application.Run(pendingTop);
        }

        private readonly List<Action> SyncActions = new List<Action>();
        private void Refresh()
        {
            foreach (var doSync in SyncActions)
                doSync();

            RenderLists();
            // todo: not sure if this is correct to call Application.Run 
            //       while the screen is already running. however, it helps
            //       keep the lists rending in the correct place when 
            //       refresh is called.
            Application.Run(currentTop);
        }

        private readonly List<Action> SaveActions = new List<Action>();
        private readonly RenderTerminalFieldFactory mapper;

        public void Save()
        {
            foreach (var doSave in SaveActions)
                doSave();
        }

        internal void AddSync(Action p)
        {
            SyncActions.Add(p);
        }

        internal void AddSave(Action p)
        {
            SaveActions.Add(p);
        }

        private ListX[] savedLists;
        public void AddLists(ListX[] listX)
        {
            savedLists = listX;
        }

        private readonly List<View> listParts = new List<View>();
        private View lastListView = null;
        public void RenderLists() 
        {
            lastListView = lastViewCreated;

            foreach(var v in listParts) {
                win.Remove(v);
            }

            listParts.Clear();

            // if(yPosListSave == 0)
            //     yPosListSave = yPos;
            // else
            //     yPos = yPosListSave;

            foreach(var list in savedLists)
            {
                list.Sync();

                var listTitle = new Label(list.Name)
                {
                    Y = Pos.Bottom(lastListView),
                    X = 0
                };
                win.Add(listTitle);
                listParts.Add(listTitle);
                lastListView = listTitle;

                foreach(var item in list.Items){
                    var listIcon = new Label("-")
                    {
                        Y = Pos.Bottom(lastListView),
                        X = 0
                    };
                    win.Add(listIcon);
                    listParts.Add(listIcon);
                    lastListView = listIcon;

                    foreach(var field in item.Fields) {
                        var mapped = mapper.Create(field);
                        mapped.Y = Pos.Top(lastListView);
                        mapped.X = Pos.Right(lastListView);
                        win.Add(mapped);
                        listParts.Add(mapped);
                        lastListView = mapped;
                    }

                    foreach(var action in item.Actions) {
                        var button = ConvertToButton(action);
                        button.Y = Pos.Top(lastListView);
                        button.X = Pos.Right(lastListView);
                        win.Add(button);
                        listParts.Add(button);
                        lastListView = button;
                    }
                }

                if (list.Items.Length == 0)
                {
                    var label = new Label("(No Records)")
                    {
                        Y = Pos.Top(lastListView),
                        X = Pos.Right(lastListView) + 1
                    };
                    win.Add(label);
                    listParts.Add(label);
                    lastListView = label;
                }
            }
        }
    }
}
