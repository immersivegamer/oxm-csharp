using System;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{
    public class MapStringToTextField : IRTFieldMapper
    {
        public Type Type => typeof(string);
        public bool RequiresLabel => true;

        public View CreateField(FieldX field, RenderTerminal renderT)
        {
            string getvalue()
            {
                return (string)field.Value() ?? "";
            }

            var textField = new TextField(getvalue())
            {
                Width = 40
            };

            renderT.AddSync(() => textField.Text = getvalue());

            if (field.HasSetter)
            {
                renderT.AddSave(() =>
                {
                    string plainText = textField.Text.ToString();
                    field.Setter(plainText);
                });
            }

            return textField;
        }
    }
}
