using System;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{
    public class MapBoolToCheckbox : IRTFieldMapper
    {
        public Type Type => typeof(bool);
        public bool RequiresLabel => false;

        public View CreateField(FieldX field, RenderTerminal renderT)
        {
            bool getvalue()
            {
                return (bool)field.Value();
            }

            var checkbox = new CheckBox(field.Name, getvalue());

            renderT.AddSync(() => checkbox.Checked = getvalue());

            if (field.HasSetter)
            {
                renderT.AddSave(() =>
                {
                    field.Setter(checkbox.Checked);
                });
            }

            return checkbox;
        }
    }
}
