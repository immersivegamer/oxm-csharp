using System;
using Terminal.Gui;
using Oxm;
using Oxm.XPart;

namespace Oxm.Render.Terminal
{
    public class MapIntToTextField : IRTFieldMapper
    {
        public Type Type => typeof(int);
        public bool RequiresLabel => true;

        public View CreateField(FieldX field, RenderTerminal renderT)
        {
            string getvalue()
            {
                return ((int)field.Value()).ToString() ?? "";
            }

            var textField = new TextField(getvalue())
            {
                Width = 40
            };

            renderT.AddSync(() => textField.Text = getvalue());

            if (field.HasSetter)
            {
                renderT.AddSave(() =>
                {
                    string plainText = textField.Text.ToString();
                    if (int.TryParse(plainText, out var t))
                        field.Setter(t);
                    else
                        textField.Text = $"Failed to parse: {plainText}";
                });
            }

            return textField;
        }
    }
}
