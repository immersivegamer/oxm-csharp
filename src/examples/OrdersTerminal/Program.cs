﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Oxm;
using Oxm.Render.Terminal;

namespace examples.order_terminal
{
    class Program
    {
        static void Main(string[] args)
        {
            Action<PersonSearch> saveAction = (ps) => System.IO.File.WriteAllText("./database.json",JsonConvert.SerializeObject(ps));
            
            var personSearch = new PersonSearch(saveAction);

            if (System.IO.File.Exists("./database.json")) {
                var stringData = System.IO.File.ReadAllText("./database.json");
                personSearch = JsonConvert.DeserializeObject<PersonSearch>(stringData);
            }

            var nav = new Navigator(new RenderTerminal(),personSearch);
            nav.Start();
        }
    }

    class PersonSearch {
        public Person[] People = new Person[] {};
        private Person[] _searchResults = new Person[] {};
        private Action<PersonSearch> _save;

        public PersonSearch(Action<PersonSearch> save)
        {
            this._save = save;
        }

        public Person New() {
            var p = new Person();
            People = People.Append(p).ToArray();
            return p;
        }

        public string SearchFirstName { get; set; }
        public string SearchLastName { get; set; }

        public void FillSearchTerms() {
            SearchFirstName = "Some ...";
            SearchLastName = "... thing!";
        }

        public void Search() {
            _searchResults = People.Where(p => p.FirstName.Contains(SearchFirstName) && p.LastName.Contains(SearchLastName)).ToArray();
        }

        public void SaveAll()  {
            _save(this);
        }

        public PersonView[] SearchResults  => _searchResults.Select(p => new PersonView(p)).ToArray();

        public PersonView[] TestLists => new PersonView[] {
            new PersonView(new Person(){FirstName="Henry", LastName = "Nitz"}),
            new PersonView(new Person(){FirstName="Km", LastName = "Sa"}),
            new PersonView(new Person(){FirstName="Li", LastName = "Al"}),
            new PersonView(new Person(){FirstName="Qw", LastName = "Pa"})
        };

        public struct PersonView {
            private Person _person;

            public PersonView(Person person)
            {
                this._person = person;
            }

            public string Name => $"{_person.FirstName}{_person.LastName}";
            public int OrderCount => _person.Orders.Count();

            public Person Open() => _person;
        }
    }

    class Person {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public Address Shipping {get; set;} = new Address();
        public Address Billing { get; set; } = new Address();

        public Order[] Orders {get; set;} = new Order[] {};

        private void AddNewOrder(Order o) {
            if (!Orders.Contains(o)) { Orders = Orders.Append(o).ToArray(); }
        }
        public Order NewOrder() {
            return new Order((o) => this.AddNewOrder(o));
        }
        public Address ViewShiping() => Shipping;
        public Address ViewBilling() => Billing;

        public OrderView[] CustomerOrders => Orders.Select(o => new OrderView(o)).ToArray();
        public struct OrderView {
            private Order _order;
            public OrderView(Order o) { _order = o; }
            public string Item => $"{_order.ItemNo}{_order.Description.Substring(0, 3)}";
            public decimal Total => _order.ExtendedPrice;
            public Order Open() => _order;
        }
    }

    public class Address {
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
    }

    public class Order
    {
        private readonly Action<Order> _save;
        private bool _final = false;

        public Order(Action<Order> save)
        {
            this._save = save;
        }

        public int ItemNo { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal ExtendedPrice => Qty * UnitPrice;
        public bool Final => _final;
        public void Save() {
            if(!_final) {
                _final = true;
                _save(this);
            }
        }
    }
}
