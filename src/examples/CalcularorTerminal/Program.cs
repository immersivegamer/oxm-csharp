﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxm;
using Oxm.Render.Terminal;

namespace calculator_terminal
{
    class Program
    {
        static void Main(string[] args)
        {
            new Navigator(new RenderTerminal(), new Calculator()).Start();
        }
    }

    class Calculator
    {
        public int ValueA { get; set; }
        public int ValueB { get; set; }
        public int Result { get; set; }
        public void Add() {
            AddHistory();
            Result = ValueA + ValueB;
        }
        private void AddHistory() {
            _history.Add(Result);
        }

        private List<int> _history = new List<int>();

        public ResultHistoryValue[] History => _history.Select( x => new ResultHistoryValue {Value = x}).Reverse().ToArray();

        public struct ResultHistoryValue {
            public int Value { get; set; }
        }
    }
}
