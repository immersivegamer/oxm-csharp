﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oxm.XPart
{
    /// <summary>
    /// Base interface to extend from for X elements
    /// </summary>
    public interface IObjectX
    {
        string Name { get; set; }        
        string Identifier { get; set; }
    }
}
