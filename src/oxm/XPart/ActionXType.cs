namespace Oxm.XPart
{
    public enum ActionXType
    {
        Command,   // object scoped only
        View,      // returns new view
        Data,      // CRUD actions for current object
        Navigation // navigate between views, e.g. closing or backwards
    }
}
