using System;

namespace Oxm.XPart
{
    public class ListX : IObjectX
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
        public Action Sync;
        public ListItemX[] Items;
    }

    public class ListItemX {
        public FieldX[] Fields;
        public ActionX[] Actions;
    }
}
