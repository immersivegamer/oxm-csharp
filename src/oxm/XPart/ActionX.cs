using System;
using Oxm.XPart;

namespace Oxm.XPart
{
    public class ActionX : IObjectX
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
        public ActionXType Type;
        public Action Execute;

        public ActionX()
        {
        }
    }
}
