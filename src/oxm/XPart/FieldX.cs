using System;

namespace Oxm.XPart
{
    public class FieldX : IObjectX
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
        public Type Type;
        public Func<object> Value;
        public Action<object> Setter;
        public bool HasSetter;
    }
}
