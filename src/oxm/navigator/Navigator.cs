﻿using Oxm.Labeler;
using Oxm.XPart;
using Oxm.Render;
using System;
using System.Linq;
using System.Reflection;

namespace Oxm
{
    public class History
    {
        public History Previous { get; set; }
        public object Obj { get; set; }
    }

    public class Navigator
    {
        private readonly IRenderX renderX;
        private readonly ILabeler labeler;
        private object entryPoint;
        private History history;

        public Navigator(IRenderX renderX, object entryPoint, ILabeler labeler = null)
        {
            this.renderX = renderX;
            this.entryPoint = entryPoint;
            this.labeler = labeler;
            if(this.labeler == null)
            {
                this.labeler = Oxm.Labeler.LabelFactory.GetDefaultLabeler();
            }
        }

        public void Start()
        {
            Show();
        }

        private void Show()
        {
            var eType = entryPoint.GetType();

            var fields = eType.GetProperties()
                .Where(pi => pi.GetMethod.IsPublic || pi.SetMethod.IsPublic)
                .Where(pi => pi.PropertyType == typeof(string) || !pi.PropertyType.IsArray)
                .Select(pi => pi.ToFieldX(entryPoint))
                .Select(pi => Label(pi));

            var actions = eType.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
                .Where(mi => !mi.IsSpecialName)
                .Where(mi => mi.DeclaringType != typeof(object))
                .Where(mi => !mi.GetParameters().Any())
                .Select(mi => mi.ToActionX(entryPoint, SwitchView))
                .Select(mi => Label(mi));

            // only get arrays that have value types for elements
            var lists = eType.GetProperties()
                .Where(pi => pi.GetMethod.IsPublic)
                .Where(pi => pi.PropertyType.IsArray && pi.PropertyType.GetElementType().IsValueType)
                .Select(pi => pi.ToListX(entryPoint, SwitchView))
                .Select(list => Label(list));

            if (history != null) {
                actions = actions.Append(new ActionX() {
                    Type = ActionXType.View,
                    Name = "Close",
                    Execute = () => { 
                        PopHistory(); 
                        Show();
                    }
                });
            }

            renderX.NewView(eType.Name);
            renderX.AddFields(fields.ToArray());
            renderX.AddActions(actions.ToArray());
            renderX.AddLists(lists.ToArray());
            renderX.Render();
        }

        private T Label<T>(T objectX) where T : IObjectX
        {
            var labelText = labeler.LookUp(objectX);
            objectX.Name = labelText.Value;
            return objectX;
        }

        private void SwitchView(object newViewObject)
        {
            if (newViewObject != null)
            {
                PushHistory();
                entryPoint = newViewObject;
                Show();
            }
        }

        private void PushHistory()
        {
            history = new History() { Previous = history, Obj = entryPoint };
        }

        private void PopHistory()
        {
            entryPoint = null;
            if (history != null)
            {
                entryPoint = history.Obj;
                history = history.Previous;
            }
        }
    }
}
