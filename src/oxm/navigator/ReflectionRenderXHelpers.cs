using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Oxm.XPart;

namespace Oxm
{
    public static class ReflectionRenderXHelpers
    {
        public static ListX ToListX(this PropertyInfo propertyInfo, object owner, Action<object> viewTrigger)
        {
            if (!propertyInfo.PropertyType.IsArray)
            {
                throw new ArgumentException("Mapped property to ListX must be an array type.");
            }

            var list = new ListX { Name = propertyInfo.Name };

            list.Sync = () =>
            {
                var array = propertyInfo.GetValue(owner) as IEnumerable;
                var arrayElementType = propertyInfo.PropertyType.GetElementType();

                var items = new List<ListItemX>();
                var elementProps = arrayElementType.GetProperties().Where(x => x.GetMethod.IsPublic);
                var elementMethods = arrayElementType.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
                    .Where(mi => !mi.IsSpecialName)
                    .Where(mi => mi.DeclaringType != typeof(object))
                    .Where(mi => !mi.GetParameters().Any());

                foreach (var element in array)
                {
                    items.Add(
                        new ListItemX
                        {
                            Fields = elementProps.Select(pi => pi.ToFieldX(element)).ToArray(),
                            Actions = elementMethods.Select(mi => mi.ToActionX(element, viewTrigger)).ToArray()
                        }
                    );
                }
                
                list.Items = items.ToArray();
            };

            list.Sync();
            return list;
        }

        public static FieldX ToFieldX(this PropertyInfo propertyInfo, object owner)
        {
            return new FieldX
            {
                Identifier = propertyInfo.CreateIdentifier(),
                Name = propertyInfo.Name,
                Type = propertyInfo.PropertyType,
                Value = () =>
                {
                    return propertyInfo.GetMethod != null && propertyInfo.GetMethod.IsPublic ?
                        propertyInfo.GetValue(owner) : null;
                },
                Setter = (change) =>
                {
                    if (propertyInfo.SetMethod != null && propertyInfo.SetMethod.IsPublic)
                    {
                        propertyInfo.SetValue(owner, change);
                    }
                },
                HasSetter = propertyInfo.SetMethod == null ? false : propertyInfo.SetMethod.IsPublic
            };
        }

        public static ActionX ToActionX(this MethodInfo methodInfo, object owner, Action<object> viewTrigger)
        {
            if (methodInfo.GetParameters().Any())
            {
                throw new ArgumentException("Mapped ActionX method cannot have parameters.");
            }

            return new ActionX
            {
                Identifier = methodInfo.CreateIdentifier(),
                Name = methodInfo.Name,
                Type = methodInfo.ReturnType.IsClass ? ActionXType.View : ActionXType.Command,
                Execute = () =>
                {
                    var result = methodInfo.Invoke(owner, null);
                    if (methodInfo.ReturnType.IsClass)
                    {
                        viewTrigger(result);
                    }
                }
            };
        }

        public static string CreateIdentifier(this MethodInfo methodInfo) => $"{methodInfo.DeclaringType.FullName}.{methodInfo.Name}";
        public static string CreateIdentifier(this PropertyInfo propertyInfo) => $"{propertyInfo.DeclaringType.FullName}.{propertyInfo.Name}";
    }
}
