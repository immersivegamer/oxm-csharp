﻿namespace Oxm.Labeler
{
    public class LabelText
    {
        public string Value { get; set; }
        public string OriginalText { get; internal set; }
        public string LookUpKey { get; internal set; }
        public string LanguageCode { get; internal set; }
    }
}