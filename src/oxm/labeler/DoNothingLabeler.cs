﻿using Oxm.XPart;

namespace Oxm.Labeler
{
    public class DoNothingLabeler : ILabeler
    {
        public string LanguageCode => "DEFAULT";

        public LabelText LookUp(IObjectX objectX)
        {
            return new LabelText
            {
                Value = objectX.Name,
                OriginalText = objectX.Name,
                LookUpKey = objectX.Identifier,
                LanguageCode = this.LanguageCode
            };
        }
    }
}