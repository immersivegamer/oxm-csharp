﻿using Oxm.XPart;
using System;
using System.Collections.Generic;
using System.Text;

namespace Oxm.Labeler
{
    public class IdentityInspector : DoNothingLabeler, ILabeler
    {
        public new string LanguageCode => "IDENTITY";

        public new LabelText LookUp(IObjectX objectX)
        {
            var labelText = base.LookUp(objectX);
            labelText.Value = objectX.Identifier;
            return labelText;
        }
    }
}
