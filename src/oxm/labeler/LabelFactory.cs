﻿using System;

namespace Oxm.Labeler
{
    internal class LabelFactory
    {
        internal static ILabeler GetDefaultLabeler()
        {
            return new DoNothingLabeler();
        }
    }
}