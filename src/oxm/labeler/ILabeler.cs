﻿using Oxm.XPart;
using System;
using System.Collections.Generic;
using System.Text;

namespace Oxm.Labeler
{
    public interface ILabeler
    {
        string LanguageCode { get; }
        LabelText LookUp(IObjectX objectX);
    }
}
