using Oxm;
using Oxm.XPart;
using System;
using System.Collections.Generic;
using System.Text;

namespace Oxm.Render {
    class ErrorInfo<Tx, TOwner> {
        public Tx objectX { get; set; }
        public Exception error { get; set; }
        public TOwner owner { get; set; }
    }

    abstract class RenderViewBase<TFieldOwner, TActionOwner, TListOwner> : IRenderX
    {        
        protected class FieldErrorInfo : ErrorInfo<FieldX, TFieldOwner> {}
        protected class ActionErrorInfo : ErrorInfo<ActionX, TActionOwner> {}
        protected class ListErrorInfo : ErrorInfo<ListX, TListOwner> {}

        private readonly List<Action> saveActions = new List<Action>();
        private List<FieldErrorInfo> warnings = new List<FieldErrorInfo>();
        private bool hasWarnings;

        public void AddActions(ActionX[] actionX)
        {
            foreach(var action in actionX)
            {
                Action saveBeforeExecute = () => { 
                    if(SaveChanges())
                        action.Execute();
                };
                HandleAddAction(action, saveBeforeExecute);
            }
        }

        protected virtual void HandleAddAction(ActionX actionX, Action saveBeforeExecute) {}

        private bool SaveChanges()
        {
            ClearWarnings();
            foreach (var save in saveActions)
                save();
            if(hasWarnings)
                HandleAllFieldWarnings();
            return !hasWarnings;
        }

        private void HandleAllFieldWarnings()
        {
            throw new NotImplementedException();
        }

        private void ClearWarnings()
        {
            foreach(var error in warnings) {
                HandleClearFieldError(error);
            }
            warnings.Clear();
            hasWarnings = false;
        }

        protected virtual void HandleClearFieldError(FieldErrorInfo error) {}

        public void AddFields(FieldX[] fieldX)
        {
            saveActions.Clear();

            for(int i = 0; i < fieldX.Length; i++)            
            {
                var field = fieldX[i];
                (TFieldOwner owner, FieldOptions options) = HandleAddField(field);
                if (field.HasSetter && !options.ForceReadOnly)
                {
                    saveActions.Add(() =>
                        {
                            bool hasModifyCheck = options.IsModified != null;
                            bool hasValueFetch = options.CurrentValue != null;

                            Func<bool> isModified = options.IsModified;
                            Func<bool> currentValue = options.CurrentValue;

                            if(hasValueFetch && (options.AlwaysSave || (hasModifyCheck && isModified())))
                            {
                                try
                                {
                                    if(options.HasCustomConverter){
                                        field.Setter(currentValue());
                                    }
                                    else{
                                        field.Setter(Convert.ChangeType(currentValue(), field.Type));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var fieldError = new FieldErrorInfo {
                                        objectX = field,
                                        error = ex,
                                        owner = owner
                                    };
                                    HandleImmediateError(fieldError);
                                    AddWarning(fieldError);
                                }
                            }
                       }
                    );
                }
            }
        }

        private (TFieldOwner owner, FieldOptions options) HandleAddField(FieldX field)
        {
            throw new NotImplementedException();
        }

        protected virtual void HandleImmediateError(FieldErrorInfo error) {}

        private void AddWarning(FieldErrorInfo error)
        {
            // todo: warnings next to fields, highlight
            this.warnings.Add(error);
            hasWarnings = true;
        }

        public void AddLists(ListX[] listX)
        {
            throw new NotImplementedException();
        }

        public abstract void NewView(string name);

        public abstract void Render();
    }

    internal class FieldOptions
    {
        public bool ForceReadOnly { get; set; }
        public Func<bool> IsModified { get; set; }
        public bool AlwaysSave { get; set; }
        public Func<bool> CurrentValue { get; set; }
        public bool HasCustomConverter { get; set; }
    }
}