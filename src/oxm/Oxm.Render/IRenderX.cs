using Oxm.XPart;

namespace Oxm.Render
{
    public interface IRenderX
    {
        // TODO: change to IEnumerable types
        void AddActions(ActionX[] actionX);
        void AddFields(FieldX[] fieldX);
        void AddLists(ListX[] listX);
        void NewView(string name);
        void Render();
    }
}
